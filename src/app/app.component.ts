import { Component } from '@angular/core';
import { ApiControllerService} from './jokes/jokes.service';
import { Router } from '@angular/router';
import { SharedService } from './shared.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title="chuck-norris";

  constructor(public apiService:ApiControllerService, public router:Router,public sharedService:SharedService) { 

  }

  ngOnInit(): void {
    
  }

}
