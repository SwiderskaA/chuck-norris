import { TestBed } from '@angular/core/testing';
import { ApiControllerService } from './jokes.service';
import {HttpClientModule} from '@angular/common/http';

describe('ApiControllerService', () => {
  let service: ApiControllerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule
      ]
    });
    service = TestBed.inject(ApiControllerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
