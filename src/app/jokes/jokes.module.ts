import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { JokesRoutingModule } from './jokes-routing.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    JokesRoutingModule
  ]
})
export class JokesModule { }
