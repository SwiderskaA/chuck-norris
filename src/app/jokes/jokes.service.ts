import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/internal/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse,HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiControllerService {
  
  endpoint="https://api.chucknorris.io/jokes/random";
  categoriesEndpoint="https://api.chucknorris.io/jokes/categories";
  
  constructor(private http: HttpClient) { }

  getCategories():Observable<any>{
    return this.http.get(this.categoriesEndpoint);
  }

  getCategoryJoke(category:string):Observable<any>{
    let urlParams = new HttpParams();
    urlParams =urlParams.set("category",category);
    return this.http.get(this.endpoint,{params:urlParams});
  }

}
