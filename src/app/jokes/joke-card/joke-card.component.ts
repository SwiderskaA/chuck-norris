import { Component, OnInit, Input } from '@angular/core';
import { ApiControllerService} from '../../jokes/jokes.service';
import { SharedService } from 'src/app/shared.service';

interface Joke{
  content:string;
  imageUrl:string;
}

@Component({
  selector: 'app-joke-card',
  templateUrl: './joke-card.component.html',
  styleUrls: ['./joke-card.component.scss']
})
export class JokeCardComponent implements OnInit {
  joke={} as Joke;
  category: string;

  constructor(public apiService: ApiControllerService, public sharedService: SharedService) { }

  ngOnInit(): void {
    this.category=this.sharedService.getCategory();
    this.getCategoryJoke(this.category);
  }

  getCategoryJoke(category: string):void{
    this.apiService.getCategoryJoke(category).subscribe((resp:any)=> {
      this.joke.content=resp.value;
      this.joke.imageUrl=resp.icon_url;
    },
    (error) => {
      console.error('error caught in component');
    }
    )
  }

  clearCategory(){
    this.sharedService.setCategory(" ");
  }
  

}
