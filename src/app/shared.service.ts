import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  category;

  constructor() { }

  setCategory(category:string){
   this.category=category;
  }

  getCategory(){
    return this.category;
  }
}
