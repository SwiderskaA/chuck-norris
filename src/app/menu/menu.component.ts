import { Component, OnInit } from '@angular/core';
import { ApiControllerService } from '../jokes/jokes.service';
import { Router } from '@angular/router';
import { SharedService } from '../shared.service';
import { Observable } from 'rxjs';
import { startWith } from 'rxjs/operators';

const  CACHE_KEY = 'httpRepoCache';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  categories$:Observable<any>;

  constructor(public apiService:ApiControllerService, public router:Router, public sharedService:SharedService) { 
    
  }

  ngOnInit(): void {
    this.getCategories();
    //cache handling
    this.categories$.subscribe(next => {
      localStorage[CACHE_KEY]=JSON.stringify(next);
    });

    this.categories$.pipe(startWith(JSON.parse(localStorage[CACHE_KEY] || '{}')));
  }

  getCategories():void{
    this.categories$=this.apiService.getCategories();
  }

  updateCategory(category:string):void{
    this.sharedService.setCategory(category);
  }

}
