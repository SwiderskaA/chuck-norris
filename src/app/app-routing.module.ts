import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {JokeCardComponent} from './jokes/joke-card/joke-card.component';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';

const routes: Routes = [
  {
    path: 'menu', component: MenuComponent,
  },

  { path: 'joke', loadChildren: () => import('./jokes/jokes.module').then(m => m.JokesModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [ MenuComponent, JokeCardComponent];
